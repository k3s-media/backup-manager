FROM ubuntu:24.04

# renovate: datasource=repology depName=ubuntu_24_04/backup-manager versioning=loose
ENV BACKUP_MANAGER_VERSION="0.7.14-1.3"

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8

RUN set -feux \
  && echo '\nAPT::Install-Recommends "false";\nAPT::Install-Suggests "false";\n' >> /etc/apt/apt.conf \
  && apt-get -y -q update \
  && apt-get -y -q dist-upgrade \
  && apt-get -y -q install backup-manager=${BACKUP_MANAGER_VERSION} dumb-init lftp \
  && rm -r /etc/backup-manager.conf \
  && touch /etc/backup-manager.conf

ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/usr/sbin/backup-manager"]
