#!/bin/bash

crc32(){
  echo -n "$1" | gzip -1 -c | tail -c8 | hexdump -n4 -e '"%u"'
}

for i in $(kubectl -n media-server get pvc -l type=config -ojsonpath='{.items[*].metadata.name}')
do
  cat $i/$i-deploy.yaml |  \
    yq '.apiVersion = "batch/v1" |
      .kind = "CronJob" |
      del (.spec.strategy, .spec.template.spec.initContainers, .spec.template.spec.containers[].resources) |
      .spec.jobTemplate.spec.template.spec =.spec.template.spec |
      del (.spec.template, .spec.jobTemplate.spec.template.spec.containers[].args, .spec.jobTemplate.spec.template.spec.containers[].command) |
      .spec.schedule = "'$(($(crc32 $i) % 59))' 2 * * *" |
      .spec.successfulJobsHistoryLimit= 0 |
      .spec.jobTemplate.spec.template.spec.affinity = {"podAffinity":{"requiredDuringSchedulingIgnoredDuringExecution":[{"labelSelector":{"matchExpressions":[{"key":"app","operator":"In","values":["'$i'"]}]},"topologyKey":"kubernetes.io/hostname"}]}} |
      .spec.jobTemplate.spec.template.spec.containers[].envFrom = [{"secretRef":{"name":"backup-manager"}}] |
      .spec.jobTemplate.spec.template.spec.containers[].env = [{"name":"HOME","value":"/tmp"},{"name":"BM_ARCHIVE_PREFIX","valueFrom":{"fieldRef":{"fieldPath":"metadata.name"}}}] |
      .spec.jobTemplate.spec.template.spec.containers[].volumeMounts = [{"mountPath":"/var/archives","name":"archives"},{"mountPath":"/config","name":"config"}] |
      .spec.jobTemplate.spec.template.spec.volumes = [{"name":"config","persistentVolumeClaim":{"claimName":"'$i'"}},{"name":"archives","emptyDir":{}}] |
      .spec.jobTemplate.spec.ttlSecondsAfterFinished = 600 |
      .spec.jobTemplate.spec.template.spec.restartPolicy = "Never" |
      .spec.jobTemplate.spec.template.spec.containers[].image = "pmialon/backup-manager"' | yq 'sort_keys(..)' | yamlfmt - | tee /tmp/$i-cj.yaml
done
